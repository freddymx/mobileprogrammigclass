package com.freddymontano.coursemanager;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ListActivity {
	
	private ProgressDialog progressDialog = null;
	private static final int ACTIVITY_CREATE=0;
	
    /** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.course_list);
        
        String[] values = new String[] { "Math","Logic","Statistics","Robotics","Mobile Programming"};
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.course_row, R.id.text1, values);
		setListAdapter(adapter);
		
        
    }
    
    public void showDialog(String title, String message){
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage("Are you sure you want to exit?")
    	       .setCancelable(true)
    	       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	                progressDialog = new ProgressDialog(MainActivity.this);
    	                progressDialog.setMessage("Working");
    	                progressDialog.show();
    	           }
    	       })
    	       .setNegativeButton("No", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	                dialog.cancel();
    	           }
    	       });
    	AlertDialog alert = builder.create();
    	alert.show();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	super.onCreateOptionsMenu(menu);
	    MenuInflater mi = getMenuInflater();
	    mi.inflate(R.menu.list_menu, menu);
	    return true;
    }
    
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
	    switch(item.getItemId()) {
	    	case R.id.menu_insert:
	    		createCourse();
	    	return true;
	    }
	    return super.onMenuItemSelected(featureId, item);
    }
    
    public boolean createCourse(){
    	//showDialog("a","a");
    	
    	Intent i = new Intent(this,CreateCourseActivity.class);
    	startActivityForResult(i, ACTIVITY_CREATE);
    	
    	return true;
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
	    super.onActivityResult(requestCode, resultCode, intent);
	    // Reload the list here
    }


    @Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		String item = (String) getListAdapter().getItem(position);
		Toast.makeText(this, item + " selected", Toast.LENGTH_SHORT).show();
	}
    
}